﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimScript : MonoBehaviour {

	private Animator anim;
	public GameObject playerSprite;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D collider){
		Debug.Log ("Entered");
		if (collider.tag == "Player") {
			Debug.Log ("Player");
			playerSprite.SetActive (false);
			anim.SetBool ("Entered", true);
		}
	}
}

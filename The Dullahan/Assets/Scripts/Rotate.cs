﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour {

	public int rotateSpeed;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey ("left")) {
			transform.Rotate (0, 0, rotateSpeed * -1);
		}
		if (Input.GetKey ("right")) {
			transform.Rotate (0, 0, rotateSpeed * -1);
		}
	}
}

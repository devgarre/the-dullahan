﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogMovement : MonoBehaviour {

	public int speed;
	public string direction;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (direction == "right") {
			transform.position += transform.right * speed * Time.deltaTime;
		}if (direction == "left") {
			transform.position -= transform.right * speed * Time.deltaTime;
		}
	}
}
